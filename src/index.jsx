import React from "react";
import { render } from "react-dom";
import { Provider } from "mobx-react";
import StockStore from "./stores/StockStore";
import StockList from "./components/StockList";

const stores = {
  stock: new StockStore()
};

render(
  <Provider store={stores}>
    <StockList />
  </Provider>,
  document.getElementById("root") // eslint-disable-line
);
