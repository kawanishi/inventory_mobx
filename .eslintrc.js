module.exports = {
  "env": {
    "es6": true
  },
  "extends": [ "airbnb", "prettier", "prettier/react" ],
  "rules": { "prettier/prettier": "error" },
  "plugins": [ "react", "jsx-a11y", "import", "prettier" ],
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaVersion": 7,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true,
      "experimentalObjectRestSpread": true
    }
  }
};
